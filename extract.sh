#!/bin/bash

function die () {
  printf "$@"
  exit 1
}

function usage () {
   printf "\tThe script requires two arguments: the input and output files\n"
}

#check that arguments were provided
[[ -z $1 ]] && usage && die "Didn't provide input file. Exiting."
[[ -z $2 ]] && usage && die "Didn't provide output file. Exiting."


# This code determines the line number of the FIRST occurrence of the string
#beginning_line=$(grep -n -m 1 "<body>" $1 |sed  's/\([0-9]*\).*/\1/')
#end_line=$(grep -n -m 1 "</body>" $1 |sed  's/\([0-9]*\).*/\1/')

# This code determines the line number of the LAST occurrence of the string
beginning_line=$(grep -n '<body>' $1 | tail -n1 | cut -d: -f1)
end_line=$(grep -n '</body>' $1 | tail -n1 | cut -d: -f1)

echo "<body> was found in line $beginning_line"
echo "</body> was found in line $end_line"

# Exclude <body> and </body> lines from the output
# to consider only the line after <body> up to the line before </body>
# this is optional but applies to this example
let beginning_line+=1
let end_line-=1

#Extract the content between beginning_line and end_line to a variable
extract=$(sed -n "$beginning_line,$end_line p" $1) 

# Write the content to the output file
# echo $extract > $2

# Write output directly to file
sed -n "$beginning_line,$end_line p" $1 > $2

